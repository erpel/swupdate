Source: swupdate
Section: embedded
Priority: optional
Maintainer: Stefano Babic <sbabic@denx.de>
Uploaders: SZ Lin (林上智) <szlin@debian.org>,
           Nobuhiro Iwamatsu <iwamatsu@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-lua <!nolua>,
               liblua5.2-dev <!nolua>,
               libfdisk-dev,
               latexmk <!nodoc>,
               libconfig-dev,
               libcurl4-openssl-dev,
               libarchive-dev,
               libjson-c-dev,
               librsync-dev,
               libssl-dev,
               libsystemd-dev,
               zlib1g-dev,
               libzstd-dev,
               libp11-kit-dev <pkg.swupdate.p11>,
               libwolfssl-dev <pkg.swupdate.p11>,
               libmtd-dev <!pkg.swupdate.bpo>,
               libubi-dev <!pkg.swupdate.bpo>,
               libwebsockets-dev (>= 3.2.0) <!pkg.swupdate.bpo>,
               liburiparser-dev <!pkg.swupdate.bpo>,
               libubootenv-dev <pkg.swupdate.uboot>,
               libcmocka-dev,
               pkg-config,
               gawk,
               python3-sphinx <!nodoc>,
               texlive-latex-recommended <!nodoc>,
               texlive-fonts-recommended <!nodoc>,
               texlive-formats-extra <!nodoc>
Standards-Version: 4.5.1
Rules-Requires-Root: no
Homepage: https://sbabic.github.io/swupdate
Vcs-Browser: https://salsa.debian.org/debian/swupdate
Vcs-Git: https://salsa.debian.org/debian/swupdate.git

Package: swupdate
Architecture: linux-any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Built-Using: ${Built-Using}
Description: Software update framework for embedded systems
 swupdate is a Linux update agent with the goal to provide an efficient and
 safe way to update an embedded system.
 .
  - Install on embedded media (eMMC, SD, Raw NAND, NOR and SPI-NOR flashes)
  - Allow delivery single image for multiple devices
  - Multiple interfaces for getting software
    1. local storage
    2. integrated web server
    3. integrated REST client connector to hawkBit
    4. remote server download
  - Software delivered as images, gzipped tarball, etc
  - Allow custom handlers for installing FPGA/ microcontroller firmware.
  - Power-Off safe

Package: libswupdate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libswupdate0.1 (= ${binary:Version})
Suggests: swupdate-doc
Breaks: swupdate-dev (<< 2020.11-1)
Replaces: swupdate-dev (<< 2020.11-1)
Description: Development files for swupdate framework
 This package contains the development files for libswupdate.
 .
 swupdate is a Linux update agent with the goal to provide an efficient and
 safe way to update an embedded system.
 .
  - Install on embedded media (eMMC, SD, Raw NAND, NOR and SPI-NOR flashes)
  - Allow delivery single image for multiple devices
  - Multiple interfaces for getting software
    1. local storage
    2. integrated web server
    3. integrated REST client connector to hawkBit
    4. remote server download
  - Software delivered as images, gzipped tarball, etc
  - Allow custom handlers for installing FPGA/ microcontroller firmware.
  - Power-Off safe

Package: libswupdate0.1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Library for controlling the swupdate framework
 This package contains the library libswupdate which is used to control
 swupdate.
 .
 swupdate is a Linux update agent with the goal to provide an efficient and
 safe way to update an embedded system.
 .
  - Install on embedded media (eMMC, SD, Raw NAND, NOR and SPI-NOR flashes)
  - Allow delivery single image for multiple devices
  - Multiple interfaces for getting software
    1. local storage
    2. integrated web server
    3. integrated REST client connector to hawkBit
    4. remote server download
  - Software delivered as images, gzipped tarball, etc
  - Allow custom handlers for installing FPGA/ microcontroller firmware.
  - Power-Off safe

Package: swupdate-doc
Section: doc
Architecture: all
Build-Profiles: <!nodoc>
Depends: ${misc:Depends}
Description: Documentation for swupdate framework
 This package contains the documentation for swupdate.
 .
 swupdate is a Linux update agent with the goal to provide an efficient and
 safe way to update an embedded system.
 .
  - Install on embedded media (eMMC, SD, Raw NAND, NOR and SPI-NOR flashes)
  - Allow delivery single image for multiple devices
  - Multiple interfaces for getting software
    1. local storage
    2. integrated web server
    3. integrated REST client connector to hawkBit
    4. remote server download
  - Software delivered as images, gzipped tarball, etc
  - Allow custom handlers for installing FPGA/ microcontroller firmware.
  - Power-Off safe

Package: lua-swupdate
Section: interpreters
Architecture: linux-any
Multi-Arch: same
Build-Profiles: <!nolua>
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: swupdate bindings for the Lua language
 This package contains the Lua bindings for swupdate.
 .
 swupdate is a Linux update agent with the goal to provide an efficient and
 safe way to update an embedded system.
 .
  - Install on embedded media (eMMC, SD, Raw NAND, NOR and SPI-NOR flashes)
  - Allow delivery single image for multiple devices
  - Multiple interfaces for getting software
    1. local storage
    2. integrated web server
    3. integrated REST client connector to hawkBit
    4. remote server download
  - Software delivered as images, gzipped tarball, etc
  - Allow custom handlers for installing FPGA/ microcontroller firmware.
  - Power-Off safe
